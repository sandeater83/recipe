//
//  ViewController.m
//  Recipe
//
//  Created by Stuart Adams on 1/21/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    int screenWidth = self.view.frame.size.width;
    int screenHeight = self.view.frame.size.height;
    
    NSArray* recipes = @[@"Ingredients \n\n 2 slices fresh white bread \n 1/4 cup milk \n 3 tablespoons clarified butter, divided \n 1/2 cup finely chopped onion \n A pinch plus 1 teaspoon kosher salt \n 3/4 pound ground chuck \n 3/4 pound ground pork \n 2 large egg yolks \n 1/2 teaspoon black pepper \n 1/4 teaspoon ground allspice \n 1/4 teaspoon freshly grated nutmeg \n 1/4 cup all-purpose flour \n 3 cups beef broth \n 1/4 cup heavy cream \n\nDirections \n\n Preheat oven to 200 degrees F. \n Tear the bread into pieces and place in a small mixing bowl along with the milk. Set aside. \n In a 12-inch straight sided saute pan over medium heat, melt 1 tablespoon of the butter. Add the onion and a pinch of salt and sweat until the onions are soft. Remove from the heat and set aside. \n In the bowl of a stand mixer, combine the bread and milk mixture, ground chuck, pork, egg yolks, 1 teaspoon of kosher salt, black pepper, allspice, nutmeg, and onions. Beat on medium speed for 1 to 2 minutes. \n Using a scale, weigh meatballs into 1-ounce portions and place on a sheet pan. Using your hands, shape the meatballs into rounds. \n Heat the remaining butter in the saute pan over medium-low heat, or in an electric skillet set to 250 degrees F. Add the meatballs and saute until golden brown on all sides, about 7 to 10 minutes. Remove the meatballs to an ovenproof dish using a slotted spoon and place in the warmed oven. \n Once all of the meatballs are cooked, decrease the heat to low and add the flour to the pan or skillet. Whisk until lightly browned, approximately 1 to 2 minutes. Gradually add the beef stock and whisk until sauce begins to thicken. Add the cream and continue to cook until the gravy reaches the desired consistency. Remove the meatballs from the oven, cover with the gravy and serve. \n\n Recipe courtesy Alton Brown, 2005 \n Read more at: http://www.foodnetwork.com/recipes/alton-brown/swedish-meatballs-recipe.html?oc=linkback", @"Ingredients \n\n3 Haas avocados, halved, seeded and peeled \n 1 lime, juiced \n 1/2 teaspoon kosher salt \n 1/2 teaspoon ground cumin \n 1/2 teaspoon cayenne \n 1/2 medium onion, diced \n 1/2 jalapeno pepper, seeded and minced \n 2 Roma tomatoes, seeded and diced \n 1 tablespoon chopped cilantro \n 1 clove garlic, minced\n\nDirections \n\n In a large bowl place the scooped avocado pulp and lime juice, toss to coat. Drain, and reserve the lime juice, after all of the avocados have been coated. Using a potato masher add the salt, cumin, and cayenne and mash. Then, fold in the onions, jalapeno, tomatoes, cilantro, and garlic. Add 1 tablespoon of the reserved lime juice. Let sit at room temperature for 1 hour and then serve. \n\n Recipe courtesy Alton Brown \n Read more at: http://www.foodnetwork.com/recipes/alton-brown/guacamole-recipe.html?oc=linkback", @"Ingredients\n\nFor the potatoes: \n 1 1/2 pounds russet potatoes \n 1/4 cup half-and-half \n 2 ounces unsalted butter \n 3/4 teaspoon kosher salt \n 1/4 teaspoon freshly ground black pepper \n 1 egg yolk \n \nFor the meat filling: \n 2 tablespoons canola oil \n 1 cup chopped onion \n 2 carrots, peeled and diced small \n 2 cloves garlic, minced \n 1 1/2 pounds ground lamb \n 1 teaspoon kosher salt \n 1/2 teaspoon freshly ground black pepper \n 2 tablespoons all-purpose flour \n 2 teaspoons tomato paste \n 1 cup chicken broth \n 1 teaspoon Worcestershire sauce \n 2 teaspoons freshly chopped rosemary leaves \n 1 teaspoon freshly chopped thyme leaves \n  1/2 cup fresh or frozen corn kernels \n 1/2 cup fresh or frozen English peas \n\n Directions \n\n Peel the potatoes and cut into 1/2-inch dice. Place in a medium saucepan and cover with cold water. Set over high heat, cover and bring to a boil. Once boiling, uncover, decrease the heat to maintain a simmer and cook until tender and easily crushed with tongs, approximately 10 to 15 minutes. Place the half-and-half and butter into a microwave-safe container and heat in the microwave until warmed through, about 35 seconds. Drain the potatoes in a colander and then return to the saucepan. Mash the potatoes and then add the half and half, butter, salt and pepper and continue to mash until smooth. Stir in the yolk until well combined. \n Preheat the oven to 400 degrees F. \n While the potatoes are cooking, prepare the filling. Place the canola oil into a 12-inch saute pan and set over medium high heat. Once the oil shimmers, add the onion and carrots and saute just until they begin to take on color, approximately 3 to 4 minutes. Add the garlic and stir to combine. Add the lamb, salt and pepper and cook until browned and cooked through, approximately 3 minutes. Sprinkle the meat with the flour and toss to coat, continuing to cook for another minute. Add the tomato paste, chicken broth, Worcestershire, rosemary, thyme, and stir to combine. Bring to a boil, reduce the heat to low, cover and simmer slowly 10 to 12 minutes or until the sauce is thickened slightly. \n Add the corn and peas to the lamb mixture and spread evenly into an 11 by 7-inch glass baking dish. Top with the mashed potatoes, starting around the edges to create a seal to prevent the mixture from bubbling up and smooth with a rubber spatula. Place on a parchment lined half sheet pan on the middle rack of the oven and bake for 25 minutes or just until the potatoes begin to brown. Remove to a cooling rack for at least 15 minutes before serving. \n\n Recipe courtesy Alton Brown, 2008 \n Read more at: http://www.foodnetwork.com/recipes/alton-brown/shepherds-pie-recipe2.html?oc=linkback", @"Ingredients\n\n 1/2 pound elbow macaroni \n 4 tablespoons butter \n 2 eggs \n 6 ounces evaporated milk \n 1/2 teaspoon hot sauce \n 1 teaspoon kosher salt \n Fresh black pepper \n 3/4 teaspoon dry mustard \n 10 ounces sharp cheddar, shredded\n\nDirections\n\nIn a large pot of boiling, salted water cook the pasta to al dente and drain. Return to the pot and melt in the butter. Toss to coat. \n Whisk together the eggs, milk, hot sauce, salt, pepper, and mustard. Stir into the pasta and add the cheese. Over low heat continue to stir for 3 minutes or until creamy. \n\n Recipe courtesy Alton Brown \n Read more at: http://www.foodnetwork.com/recipes/alton-brown/stove-top-mac-n-cheese-recipe.html?oc=linkback", @"Ingredients\n\n 6 ounces garlic-flavored croutons \n 1/2 teaspoon ground black pepper \n 1/2 teaspoon cayenne pepper \n 1 teaspoon chili powder \n 1 teaspoon dried thyme \n 1/2 onion, roughly chopped \n 1 carrot, peeled and broken \n 3 whole cloves garlic \n 1/2 red bell pepper \n 18 ounces ground chuck \n 18 ounces ground sirloin \n 1 1/2 teaspoon kosher salt \n 1 egg \n\n For the glaze: \n 1/2 cup catsup \n 1 teaspoon ground cumin \n Dash Worcestershire sauce \n Dash hot pepper sauce \n 1 tablespoon honey \n Directions \n\n Heat oven to 325 degrees F. \n In a food processor bowl, combine croutons, black pepper, cayenne pepper, chili powder, and thyme. Pulse until the mixture is of a fine texture. Place this mixture into a large bowl. Combine the onion, carrot, garlic, and red pepper in the food processor bowl. Pulse until the mixture is finely chopped, but not pureed. Combine the vegetable mixture, ground sirloin, and ground chuck with the bread crumb mixture. Season the meat mixture with the kosher salt. Add the egg and combine thoroughly, but avoid squeezing the meat. \n Pack this mixture into a 10-inch loaf pan to mold the shape of the meatloaf. Onto a parchment paper-lined baking sheet, turn the meatloaf out of the pan onto the center of the tray. Insert a temperature probe at a 45 degree angle into the top of the meatloaf. Avoid touching the bottom of the tray with the probe. Set the probe for 155 degrees. \n Combine the catsup, cumin, Worcestershire sauce, hot pepper sauce and honey. Brush the glaze onto the meatloaf after it has been cooking for about 10 minutes. \n\n Recipe courtesy of Alton Brown \n Read more at: http://www.foodnetwork.com/recipes/alton-brown/good-eats-meat-loaf-recipe.html?oc=linkback"];
    
    NSArray* recipeNames = @[@"Swedish Meatballs", @"Guacamole", @"Shepard's Pie", @"Mac-and-Cheese", @"Meatloaf"];
    
    NSArray* imgNames = @[@"Swedish Meatball", @"guacamole", @"shepard pie", @"mac and cheese", @"meatloaf"];

    
    UIScrollView* mainView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    
    [mainView setContentSize:CGSizeMake(screenWidth * 5, screenHeight)];
    
    mainView.backgroundColor = [UIColor whiteColor];
    mainView.pagingEnabled = TRUE;
    
    [self.view addSubview:mainView];
    
    //adding views
    for(int i = 0; i < 5; i++)
    {
        UIScrollView* view = [[UIScrollView alloc]initWithFrame:CGRectMake(screenWidth * i, 0, screenWidth, screenHeight)];
                UIImageView* imgView;
        UILabel* title;
        UITextView* recipeTxt;
        
        recipeTxt.editable = false;
        
        recipeTxt = [[UITextView alloc]initWithFrame:CGRectMake(10, 160, screenWidth - 20, screenHeight/2 + 100)];
        recipeTxt.text = recipes[i];
        
        if(i == 2)
        {
            imgView = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth/2 - 45, 30, 90, 90)];
            
            title = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2 - 75, 130, 150, 20)];
            [view setContentSize:CGSizeMake(screenWidth, screenHeight * 1.5)];
        }
        else
        {
            title = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2 - 75, 30, 150, 20)];
            
            imgView = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth/2 - 45, 60, 90, 90)];
            [view setContentSize:CGSizeMake(screenWidth, screenHeight* 2 )];
            
            [recipeTxt sizeToFit];
        }
        
        imgView.image = [UIImage imageNamed:imgNames[i]];
        
        view.backgroundColor = [UIColor colorWithRed:(i * .2) green:(.3) blue:(.4) alpha:1];
        
        title.text = recipeNames[i];
        title.textAlignment = NSTextAlignmentCenter;
        imgView.backgroundColor = [UIColor whiteColor];

        [mainView addSubview:view];
        [view addSubview:title];
        [view addSubview:imgView];
        [view addSubview:recipeTxt];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
